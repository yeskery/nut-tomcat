# nut-tomcat

#### 介绍
Nut-Tomcat提供Tomcat所需要的完整依赖

#### 启动方式
```java
@NutBootApplication(serverType = ServerType.TOMCAT)
public class DemoApplication {

    /**
     * 启动主方法
     * @param args 启动参数
     */
    public static void main(String[] args) {
        NutConfiguration nutConfiguration = NutConfigurationBuilder.newInstance()
                .setMainClass(DemoApplication.class)
                .setLogLevel(Level.FINER)
                .build();
        Nut.run(nutConfiguration, args);
    }
}
```